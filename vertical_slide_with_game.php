<div class="swiper-entry slider-2">
    <div class="swiper-container full-screen-height" data-direction="vertical" data-parallax="1" data-mousewheel="1" data-speed="1000">
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="container-fluid">
                    <div class="game-holder" id="gameHolder">
                        <div class="title-wrapper">
                            <div class="content">
                                <div class="h1 title text-center" style="color: #e7ad01;"><span class="ht-1"><a href="#">HI Im Shazzadur Rahaman</a></span></div>
                            </div>
                        </div>
                        <div class="world" id="world"></div>
                        <div class="message message--replay" id="replayMessage">Click to Replay</div>
                        <div class="message message--instructions" id="instructions">
                            <div class="score" id="score">
                                <div class="score__content" id="level">
                                    <div class="score__label">level</div>
                                    <div class="score__value score__value--level" id="levelValue">1</div>
                                    <svg class="level-circle" id="levelCircle" viewbox="0 0 200 200">
                                        <circle id="levelCircleBgr" r="80" cx="100" cy="100" fill="none" stroke="#d1b790" stroke-width="24px" />
                                        <circle id="levelCircleStroke" r="80" cx="100" cy="100" fill="none" #f25346 stroke="#68c3c0" stroke-width="14px" stroke-dasharray="502" />
                                    </svg>
                                </div>
                                <div class="score__content" id="dist">
                                    <div class="score__label">distance</div>
                                    <div class="score__value score__value--dist" id="distValue">000</div>
                                </div>
                                <div class="score__content" id="energy">
                                    <div class="score__label">energy</div>
                                    <div class="score__value score__value--energy" id="energyValue">
                                        <div class="energy-bar" id="energyBar"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-pagination visible-xs"></div>
        <div class="swiper-pager hidden-xs">
            <div class="swiper-pager-current">01</div>
            <div class="swiper-pager-total">03</div>
            <div class="swiper-pager-arrow-prev"></div>
            <div class="swiper-pager-arrow-next"></div>
        </div>
    </div>
</div>






<!--<div class="swiper-slide">-->
<!--    <div class="container-fluid wide">-->
<!--        <div class="image-preview image-1" data-swiper-parallax-y="-55%" style="background-image: url(img/thumbnail-94.jpg);"><div class="content"></div></div>-->
<!--        <div class="image-preview image-2" data-swiper-parallax-y="-35%" style="background-image: url(img/thumbnail-95.jpg);"><div class="content"></div></div>-->
<!--        <div class="image-preview image-3" data-swiper-parallax-y="-70%" style="background-image: url(img/thumbnail-96.jpg);"><div class="content"></div></div>-->
<!--        <div class="image-preview image-4" data-swiper-parallax-y="-40%" style="background-image: url(img/thumbnail-97.jpg);"><div class="content"></div></div>-->
<!--        <div class="sl-entry sl-1">-->
<!--            <div class="sl">For: LemonType Studio</div>-->
<!--        </div>-->
<!--        <div class="sl-entry sl-2">-->
<!--            <div class="sl">Branding / Typography</div>-->
<!--        </div>-->
<!--        <div class="title-wrapper valign-middle">-->
<!--            <div class="content">-->
<!--                <div class="h1 title text-center" style="color: #e7ad01;"><span class="ht-1"><a href="#">Dive in Philosophical Underground</a></span></div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<!--<div class="swiper-slide">-->
<!--    <div class="container-fluid wide">-->
<!--        <div class="image-preview image-1" data-swiper-parallax-y="-55%" style="background-image: url(img/thumbnail-94_.jpg);"><div class="content"></div></div>-->
<!--        <div class="image-preview image-2" data-swiper-parallax-y="-35%" style="background-image: url(img/thumbnail-95_.jpg);"><div class="content"></div></div>-->
<!--        <div class="image-preview image-3" data-swiper-parallax-y="-70%" style="background-image: url(img/thumbnail-96_.jpg);"><div class="content"></div></div>-->
<!--        <div class="image-preview image-4" data-swiper-parallax-y="-40%" style="background-image: url(img/thumbnail-97_.jpg);"><div class="content"></div></div>-->
<!--        <div class="sl-entry sl-1">-->
<!--            <div class="sl">For: LemonType Studio</div>-->
<!--        </div>-->
<!--        <div class="sl-entry sl-2">-->
<!--            <div class="sl">Branding / Typography</div>-->
<!--        </div>-->
<!--        <div class="title-wrapper valign-middle">-->
<!--            <div class="content">-->
<!--                <div class="h1 title text-center" style="color: #e7ad01;"><span class="ht-1"><a href="#">Dive in Philosophical Underground</a></span></div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="swiper-slide">-->
<!--    <div class="container-fluid wide">-->
<!--        <div class="image-preview image-1" data-swiper-parallax-y="-55%" style="background-image: url(img/thumbnail-94__.jpg);"><div class="content"></div></div>-->
<!--        <div class="image-preview image-2" data-swiper-parallax-y="-35%" style="background-image: url(img/thumbnail-95__.jpg);"><div class="content"></div></div>-->
<!--        <div class="image-preview image-3" data-swiper-parallax-y="-70%" style="background-image: url(img/thumbnail-96__.jpg);"><div class="content"></div></div>-->
<!--        <div class="image-preview image-4" data-swiper-parallax-y="-40%" style="background-image: url(img/thumbnail-97__.jpg);"><div class="content"></div></div>-->
<!--        <div class="sl-entry sl-1">-->
<!--            <div class="sl">For: LemonType Studio</div>-->
<!--        </div>-->
<!--        <div class="sl-entry sl-2">-->
<!--            <div class="sl">Branding / Typography</div>-->
<!--        </div>-->
<!--        <div class="title-wrapper valign-middle">-->
<!--            <div class="content">-->
<!--                <div class="h1 title text-center" style="color: #e7ad01;"><span class="ht-1"><a href="#">Dive in Philosophical Underground</a></span></div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->