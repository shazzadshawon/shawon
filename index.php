<!DOCTYPE html>
<html lang="en">
<head>
    <title>Shazzad|Creative </title>
    <?php require('meta.php'); ?>
    <?php require('head.php'); ?>

    <link rel="stylesheet" type="text/css" href="css/shazzad.css">

    <script>var currPage="";</script>
</head>
<body>

    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId            : '1909853295922446',
          autoLogAppEvents : true,
          xfbml            : false,
          version          : 'v2.10'
        });
        FB.AppEvents.logPageView();
        FB.api('/me/feed', 'post', {message: 'Hello, world!'});
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>

    <!-- <div id="fb-root"></div>
    <div id="fb-content"></div> -->

    <!-- LOADER -->
    <div id="loader-wrapper"></div>

    <!-- HEADER -->
    <?php require('header.php'); ?>



    <!-- START HOME SLIDER -->
    <?php require('home_slider.php'); ?>
    <!-- END HOME SLIDER -->

    <div class="sec-loader">
        <div class="sec-loader-display">
            <div class="sec-loader-gauge"><div class="loading-bar-gauge"></div></div>
            <div class="sec-loader-info">Loading</div>
        </div>
    </div>

    <!-- FOOTER -->
    <?php require('footer.php'); ?>
    <!-- FOOTER -->

    <!--START POPUP CONTENTS-->
    <?php require('popupContent.php'); ?>
    <!--END POPUP CONTENTS-->

    <div class="phone-marker visible-xs"></div><div class="tablet-marker visible-sm"></div>

    <?php require('tail.php'); ?>
    <!-- <script type="text/javascript" src="js/facebook.js"></script> -->

<!--    <script type="text/javascript">-->
<!---->
<!--        var _gaq = _gaq || [];-->
<!--        _gaq.push(['_setAccount', 'UA-40095219-1']);-->
<!--        _gaq.push(['_trackPageview']);-->
<!---->
<!--        (function() {-->
<!--            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;-->
<!--            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';-->
<!--            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);-->
<!--        })();-->
<!---->
<!--    </script>-->

</body>
</html>
