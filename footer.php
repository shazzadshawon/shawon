<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-xs-text-center col-md-text-right" style="float: right;">
                <div class="copyright sa small">&copy; 2017 All rights reserved. Developed With <i class="fa fa-heart" aria-hidden="true"></i> by Me. </div>
                <div class="col-xs-b15 visible-xs visible-sm"></div>
            </div>
            <!-- <div id="footer_social" class="col-md-6 col-xs-text-center col-md-text-right">
                <div class="follow style-1">
                    <a class="entry" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a class="entry" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a class="entry" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
                    <a class="entry" href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a>
                </div>
            </div> -->
        </div>
    </div>
</footer>