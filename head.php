<link rel="shortcut icon" href="img/favicon.ico" />

<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900" rel="stylesheet" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.extension.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/swiper.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/game.css" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />

