<div id="container">

        <!-- Site sections -->
        <div id="screens">

            <!-- Home -->
            <section id="a-home" class="screen">

                <div id="home-statements">


                    <div class="statement-spinner theme-yellow">
                        <div class="statement">
                            <div class="st-title">HI, IM Shazzad</div>
                            <div class="st-desc">I'm a software engineer &amp; UI/UX developer currently working @ AGV BD Limited.</div>
<!--                            <div class="st-learn-more"><a class="flp-btn learn-more" href="/about" target="_self" title="Learn more" rel="address:/about"><div class="btn"></div></a></div>-->
                        </div>
                    </div>

                    <div class="statement-spinner theme-pink">
                        <div class="statement">
                            <div class="st-title">I Love Javascript</div>
                            <div class="st-desc">I specialise in rich interactive experiences for web, mobile and tablet devices.</div>
<!--                            <div class="st-learn-more"><a class="flp-btn learn-more" href="/about" target="_self" title="Learn more" rel="address:/about"><div class="btn"></div></a></div>-->
                        </div>
                    </div>

                    <div class="statement-spinner theme-blue">
                        <div class="statement">
                            <div class="st-title">I have Good Friends</div>
                            <div class="st-desc">I love to collaborate. I'm currently looking for other creative minds to work, grow and network. Get in touch!</div>
<!--                            <div class="st-learn-more"><a class="flp-btn learn-more" href="/contact" target="_self" title="Learn more" rel="address:/contact"><div class="btn"></div></a></div>-->
                        </div>
                    </div>

                    <div class="statement-spinner theme-black">
                        <div class="statement">
                            <div class="st-title">IM Good At My Work</div>
                            <div class="st-desc">Being multi disciplined is a big part of my ethos. Whether it's design, development, video or UX; I'm there.</div>
<!--                            <div class="st-learn-more"><a class="flp-btn learn-more" href="/skills" target="_self" title="Learn more" rel="address:/skills"><div class="btn"></div></a></div>-->
                        </div>
                    </div>

                    <div class="statement-spinner theme-lightgray">
                        <div class="statement">
                            <div class="st-title">Adam, You are hacked</div>
                            <div class="st-desc">My focus is working on engaging and interactive applications across the spectrum of devices and platforms.</div>
<!--                            <div class="st-learn-more"><a class="flp-btn learn-more" href="/work-and-play" target="_self" title="Learn more" rel="address:/work-and-play"><div class="btn"></div></a></div>-->
                        </div>
                    </div>

                    <div class="statement-spinner theme-yellow">
                        <div class="statement">
                            <div class="st-title">I love to play</div>
                            <div class="st-desc">Work makes up such a big part of our lives, so it's important to integrate it in a positive way.</div>
<!--                            <div class="st-learn-more"><a class="flp-btn learn-more" href="/work-and-play" target="_self" title="Learn more" rel="address:/work-and-play"><div class="btn"></div></a></div>-->
                        </div>
                    </div>

                    <div class="statement-spinner theme-yellow">
                        <div class="statement">
                            <div class="st-title">Creative Edge Parties</div>
                            <div class="st-desc">The experimental site of Creative Edge Parties. Not Just Another Chicken Dinner.</div>
<!--                            <div class="st-learn-more"><a class="flp-btn learn-more" href="http://www.creativeedgeparties.com" target="_blank" title="Learn more" ><div class="btn"></div></a></div>-->
                        </div>
                    </div>

                    <div class="statement-spinner theme-black">
                        <div class="statement">
                            <div class="st-title">Citak Rugs Inc</div>
                            <div class="st-desc">Solo project for Citak.</div>
<!--                            <div class="st-learn-more"><a class="flp-btn learn-more" href="http://www.citakrugs.ca" target="_blank" title="Learn more" ><div class="btn"></div></a></div>-->
                        </div>
                    </div>

                    <div class="statement-spinner theme-green">
                        <div class="statement">
                            <div class="st-title">Apps apps apps!</div>
                            <div class="st-desc">Reaching audiences on the go is becoming increasingly important; I build Apps for both Web and native.</div>
<!--                            <div class="st-learn-more"><a class="flp-btn learn-more" href="/work-and-play" target="_self" title="Learn more" rel="address:/work-and-play"><div class="btn"></div></a></div>-->
                        </div>
                    </div>

                    <div class="statement-spinner theme-blue">
                        <div class="statement">
                            <div class="st-title">Interested?</div>
                            <div class="st-desc">If you've got an exciting project you're interested in working together on, get in touch!</div>
<!--                            <div class="st-learn-more"><a class="flp-btn learn-more" href="/contact" target="_self" title="Learn more" rel="address:/contact"><div class="btn"></div></a></div>-->
                        </div>
                    </div>


                </div>

            </section>
            <!-- End Home -->
        </div>
        <!-- End Site sections -->
    </div>