<?php
// set up autoloader
require __DIR__.'/vendor/autoload.php';

// configure database
$dsn = 'mysql:dbname=shazzadur;host=localhost';
$u = 'root';
$p = '';
Cartalyst\Sentry\Facades\Native\Sentry::setupDatabaseResolver( new PDO($dsn, $u, $p) );

require 'cors_header.php';

session_start();
$header_location = 'http://kaporchupor.com/plexpos';
$useractivationemail;

require 'flight/Flight.php';

require 'lib/EasyPDO.php';

Flight::register('db', 'EasyPDO', array('mysql:dbname=shazzadur;host=localhost;charset=UTF8', "root", ""));

Flight::route('/checkLogin', function(){
    if ( ! Cartalyst\Sentry\Facades\Native\Sentry::check()){
        $output = array( 'status' => false );
        Flight::json($output);
    }
    else{
        $currentUser = Cartalyst\Sentry\Facades\Native\Sentry::getLogin();
        $currentUserDetails = Cartalyst\Sentry\Facades\Native\Sentry::findUserByLogin($currentUser);
        Flight::json(array(
            "status"=> true,
            "id"=> $currentUserDetails['id'],
            "first_name"=> $currentUserDetails['first_name'],
            "last_name"=> $currentUserDetails['last_name'],
            "last_login"=> $currentUserDetails['last_login'],
        ));
    }
});

Flight::map('checkLogin', function(){
    if ( ! Cartalyst\Sentry\Facades\Native\Sentry::check()){
        // User is not logged in, or is not activated
        $output = array(
            'status' => 'login'
        );
        Flight::json($output);
    }
    else{
        // User is logged in
    }
});

Flight::route('/', function(){
    echo "hello world";
});

Flight::route('/docroot', function(){
    echo $_SERVER['HTTP_HOST'];
});

Flight::route('/user/test/save', function(){
    //Flight::checkLogin();

    $db = Flight::db();

    $db->insert("users", array('email'=>'mas@kiteplexit.com','password'=>md5('administrator_super_admin')));

    Flight::json(array(
        'status'  => 'success',
        'message' => 'User Created Successfully.'
    ));
});

Flight::route('POST /login', function(){
    $entityBody = file_get_contents('php://input');
    $data = json_decode($entityBody);

    try {
        // validate input
        $email = filter_var($data->email, FILTER_SANITIZE_EMAIL);
        $password = strip_tags(trim($data->password));

        // set login credentials
        $credentials = array(
          'email'    => $email,
          'password' => $password,
        );
        // authenticate
        $currentUser = Cartalyst\Sentry\Facades\Native\Sentry::authenticate($credentials, false);
        $user = Cartalyst\Sentry\Facades\Native\Sentry::findUserByLogin($currentUser->getLogin());

        Cartalyst\Sentry\Facades\Native\Sentry::login($user, false);

        Flight::json(array(
            'status'  => 'success',
            'message' => 'Logged in as ' . $currentUser->getLogin(),
            'token' => md5(md5($email))
        ));
    } catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
    {
        Flight::json(array(
            'status'  => 'error',
            'message' => 'Login field is required'
        ));
    }
    catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
    {
        Flight::json(array(
            'status'  => 'error',
            'message' => 'Password field is required.'
        ));
    }
    catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
    {
        Flight::json(array(
            'status'  => 'error',
            'message' => 'Wrong password, try again.'
        ));
    }
    catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
    {
        Flight::json(array(
            'status'  => 'error',
            'message' => 'User was not found.'
        ));
    }
    catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
    {
        Flight::json(array(
            'status'  => 'error',
            'message' => 'User is not activated.'
        ));
    }
    catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
    {
        Flight::json(array(
            'status'  => 'error',
            'message' => 'User is suspended.'
        ));
    }
    catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
    {
        Flight::json(array(
            'status'  => 'error',
            'message' => 'User is banned.'
        ));
    }
});

Flight::route('/user/new', function(){
    try {

        $user = Cartalyst\Sentry\Facades\Native\Sentry::createUser(array(
            'email'    => 'shazzad@gmail.com',
            'password' => 'sha@12',
            'first_name' => 'shazzad',
            'last_name' => 'shawon',
            'activated' => true,
        ));

    } catch (Exception $e) {
        echo $e->getMessage();
    }
});

//// User end

// ..............PAGES END.................

Flight::start();
?>
