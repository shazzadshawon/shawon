<header class="type-1">
    <div class="header-wrapper">
        <a id="logo" href="index1.html"><img src="img/logo.png" alt="" /></a>
        <div class="hamburger-icon">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
        <div class="follow style-1">
            <a class="entry" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
            <a class="entry" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
            <a class="entry" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
            <a class="entry" href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a>
        </div>
    </div>
    <div class="navigation-wrapper">
        <nav>
            <ul>
                <li>
                    <a href="index.php">Home</a>
                </li>
                <li>
                    <a href="about.php">About Me</a>
                </li>
                <!-- <li>
                    <a href="portfolio1.html">Portfolio</a>
                    <div class="toggle-menu"></div>
                    <ul>
                        <li><a href="portfolio1.html">Portfolio landing 1</a></li>
                        <li><a href="portfolio2.html">Portfolio landing 2</a></li>
                        <li><a href="portfolio3.html">Portfolio landing 3</a></li>
                        <li><a href="portfolio4.html">Portfolio landing 4</a></li>
                        <li><a href="portfolio5.html">Portfolio landing 5</a></li>
                        <li><a href="portfoliodetail1.html">Portfolio detail</a></li>
                    </ul>
                </li> -->
                <li>
                    <a href="portfolio.php">Works</a>
                </li>
                <li>
                    <a href="blog.php">Blog</a>
                </li>
                <li>
                    <a href="contact1.html">Get In Touch</a>
                </li>
                <!-- <li>
                    <a href="shop1.html">Shop</a>
                    <div class="toggle-menu"></div>
                    <ul>
                        <li><a href="shop1.html">Shop sidebar left</a></li>
                        <li><a href="shop2.html">Shop sidebar right</a></li>
                        <li><a href="shop3.html">Shop no sidebar</a></li>
                        <li><a href="shop4.html">Shop full width</a></li>
                        <li><a href="shopdetail1.html">Shop detail sidebar left</a></li>
                        <li><a href="shopdetail2.html">Shop detail sidebar right</a></li>
                        <li><a href="shopdetail3.html">Shop detail no sidebart</a></li>
                        <li><a href="cart.html">Shopping cart</a></li>
                        <li><a href="checkout.html">Checkout</a></li>
                    </ul>
                </li> -->

                <!-- LOGIN -->
                <!-- <li> <a class="open-popup" data-rel="1"><i class="fa fa-user" aria-hidden="true"></i> Login</a> </li> -->
                <!-- LOGIN -->
            </ul>
        </nav>
    </div>
</header>