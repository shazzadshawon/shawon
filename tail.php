<script type="text/javascript" src="js/modernizer-2.6.2.min.js"></script>
<script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>

<script type="text/javascript" src="js/core.js"></script>
<script type="text/javascript" src="js/jquery.ui.js"></script>

<script type="text/javascript" src="js/swiper.jquery.min.js"></script>
<script type="text/javascript" src="js/TweenMax.min.js"></script>
<script type="text/javascript" src="js/three.min.js"></script>
<!--<script type="text/javascript" src="js/game.js"></script>-->
<script src="js/global.js"></script>

<script src="js/isotope.pkgd.min.js"></script>

<!-- lightbox -->
<link href="css/simplelightbox.css" rel="stylesheet" type="text/css" />
<script src="js/simple-lightbox.min.js"></script>
