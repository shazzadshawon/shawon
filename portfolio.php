<!DOCTYPE html>
<html lang="en">
<head>
    <title>Shazzad|Works </title>
    <?php require('meta.php'); ?>
    <?php require('head.php'); ?>
</head>
<body>

<!-- LOADER -->
<div id="loader-wrapper"></div>

<!-- HEADER -->
<?php require('header.php'); ?>
<!-- //HEADER -->

<div id="content-block">

    <div class="container-fluid">

        <div class="empty-space col-xs-b40 col-sm-b80"></div>

        <div class="row">
            <div class="col-md-6 col-md-offset-1">
                <article class="sa">
                    <h3>Some Silly Works of Mine</h3>
                </article>
                <div class="empty-space col-xs-b25 col-sm-b50"></div>
            </div>
            <div class="col-md-11 col-md-offset-1">
                <div class="sorting-menu">
                    <div class="title">All</div>
                    <div class="toggle">
                        <a class="active" data-filter="*"><span class="text">All</span><span class="number">184</span></a>
                        <a data-filter=".filter-1"><span class="text">Branding</span><span class="number">34</span></a>
                        <a data-filter=".filter-2"><span class="text">Typography</span><span class="number">25</span></a>
                        <a data-filter=".filter-3"><span class="text">Logo</span><span class="number">18</span></a>
                        <a data-filter=".filter-4"><span class="text">Marketing</span><span class="number">40</span></a>
                        <a data-filter=".filter-5"><span class="text">Graphic Design</span><span class="number">67</span></a>
                    </div>
                </div>
                <div class="empty-space col-xs-b25 col-sm-b50"></div>
            </div>
        </div>


        <div class="sorting-container portfolio-7">
            <div class="grid-sizer w50"></div>

            <div class="sorting-item w50 filter-1">
                <div class="portfolio-preview-7">
                    <a class="mouseover-1 lightbox" href="img/thumbnail-76.jpg">
                        <img src="img/thumbnail-76.jpg" alt="" />
                        <img src="img/thumbnail-76.jpg" alt="" />
                    </a>
                    <div class="h6 title"><span class="ht-2"><a href="#">Absolute meger with nature world</a></span></div>
                    <div class="sl">Graphic Design / Illustration</div>
                </div>
            </div>

            <div class="sorting-item w50 filter-2">
                <div class="portfolio-preview-7">
                    <a class="mouseover-1 lightbox" href="img/thumbnail-77.jpg">
                        <img src="img/thumbnail-77.jpg" alt="" />
                        <img src="img/thumbnail-77.jpg" alt="" />
                    </a>
                    <div class="h6 title"><span class="ht-2"><a href="#">All you have, just sticks and stones</a></span></div>
                    <div class="sl">Typography / Print Design</div>
                </div>
            </div>

            <div class="sorting-item w50 filter-3">
                <div class="portfolio-preview-7">
                    <a class="mouseover-1 lightbox" href="img/thumbnail-78.jpg">
                        <img src="img/thumbnail-78.jpg" alt="" />
                        <img src="img/thumbnail-78.jpg" alt="" />
                    </a>
                    <div class="h6 title"><span class="ht-2"><a href="#">Ocean life of magnetic shells</a></span></div>
                    <div class="sl">Typography / Print Design</div>
                </div>
            </div>

            <div class="sorting-item w50 filter-4">
                <div class="portfolio-preview-7">
                    <a class="mouseover-1 lightbox" href="img/thumbnail-79.jpg">
                        <img src="img/thumbnail-79.jpg" alt="" />
                        <img src="img/thumbnail-79.jpg" alt="" />
                    </a>
                    <div class="h6 title"><span class="ht-2"><a href="#">Whithout nature there’s no future</a></span></div>
                    <div class="sl">Poster Design / Typography</div>
                </div>
            </div>

            <div class="sorting-item w50 filter-5">
                <div class="portfolio-preview-7">
                    <a class="mouseover-1 lightbox" href="img/thumbnail-80.jpg">
                        <img src="img/thumbnail-80.jpg" alt="" />
                        <img src="img/thumbnail-80.jpg" alt="" />
                    </a>
                    <div class="h6 title"><span class="ht-2"><a href="#">Mystic value of African mask</a></span></div>
                    <div class="sl">Poster Design / Typography</div>
                </div>
            </div>

            <div class="sorting-item w50 filter-2">
                <div class="portfolio-preview-7">
                    <a class="mouseover-1 lightbox" href="img/thumbnail-81.jpg">
                        <img src="img/thumbnail-81.jpg" alt="" />
                        <img src="img/thumbnail-81.jpg" alt="" />
                    </a>
                    <div class="h6 title"><span class="ht-2"><a href="#">Line philosophi in Greece sculpture</a></span></div>
                    <div class="sl">Graphic Design / Illustration</div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- FOOTER -->
<?php require('footer.php'); ?>
<!-- FOOTER -->

<!--START POPUP CONTENTS-->
<?php require('popupContent.php'); ?>
<!--END POPUP CONTENTS-->

<div class="phone-marker visible-xs"></div><div class="tablet-marker visible-sm"></div>

<?php require('tail.php'); ?>

</body>
</html>